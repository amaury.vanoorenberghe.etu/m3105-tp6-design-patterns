package fr.univlille.iutinfo.m3105.patisserie.dessert;

public class EmptyDessert extends Dessert {
	@Override
	public double price() {
		return Double.NaN;
	}

	@Override
	public String name() {
		return "Le dessert ne doit pas commencer par une garniture";
	}

	@Override
	public String[] identifier() {
		return new String[0];
	}
}
