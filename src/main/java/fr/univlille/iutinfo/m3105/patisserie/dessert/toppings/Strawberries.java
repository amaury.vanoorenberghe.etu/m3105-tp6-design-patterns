package fr.univlille.iutinfo.m3105.patisserie.dessert.toppings;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.Topping;

/**
 * Fraises
 */
public class Strawberries extends Topping {
	public Strawberries(Dessert base) {
		super(base);
	}

	@Override
	public double extraPrice() {
		return 1.25;
	}

	@Override
	public String nameFormat() {
		return "%s aux fraises";
	}

	@Override
	public String[] identifier() {
		return new String[] {"fraises", "strawberries"};
	}
}
