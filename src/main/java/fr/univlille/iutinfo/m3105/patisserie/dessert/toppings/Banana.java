package fr.univlille.iutinfo.m3105.patisserie.dessert.toppings;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.Topping;

/**
 * P O T A S S I U M
 */
public class Banana extends Topping {
	public Banana(Dessert base) {
		super(base);
	}

	@Override
	public double extraPrice() {
		return 1.1;
	}

	@Override
	public String nameFormat() {
		return "%s avec des morceaux de banane";
	}

	@Override
	public String[] identifier() {
		return new String[] {"potassium", "babane", "banana"};
	}
}
