package fr.univlille.iutinfo.m3105.patisserie.dessert;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Supplier;

import fr.univlille.iutinfo.m3105.patisserie.dessert.bases.Crepe;
import fr.univlille.iutinfo.m3105.patisserie.dessert.bases.Waffle;
import fr.univlille.iutinfo.m3105.patisserie.dessert.toppings.*;

public enum Desserts {
	NONE			(EmptyDessert::new),
	WAFFLE			(Waffle::new),
	CREPE			(Crepe::new),
	CHOCOLATE		(Chocolate::new),
	STRAWBERRIES	(Strawberries::new),
	BANANA			(Banana::new),
	CARAMEL			(Caramel::new),
	WHIPPED_CREAM	(WhippedCream::new);
	
	private static final Dessert NOTHING = new EmptyDessert();
	private final Function<Dessert, Dessert> CONSTRUCTOR; 
	
	private Desserts(Function<Dessert, Dessert> ctor) {
		CONSTRUCTOR = ctor;
	}
	
	private Desserts(Supplier<Dessert> ctor) {
		CONSTRUCTOR = (d) -> ctor.get();
	}
	
	public static final Desserts fromName(String name) {
		return Arrays.stream(values())
		.filter(item -> Arrays.stream(item.identifier())
			.anyMatch(name.toLowerCase()::equals))
		.findFirst().orElse(NONE);
	}
	
	public Dessert make(Dessert base) {
		try {
			return CONSTRUCTOR.apply(base);	
		} catch (Exception e) {
			return new EmptyDessert();
		}
	}
	
	public String[] identifier() {
		return make(NOTHING).identifier();
	}
}