package fr.univlille.iutinfo.m3105.patisserie.printers;

import java.util.Locale;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;

public class DessertPrinterSingleLine extends DessertPrinter {
	@Override
	public String print(Dessert dessert) {
		final String fullName = dessert.name();
		final String fullPrice = String.format(Locale.ROOT, "%.2f€", dessert.price());
		
		StringBuilder sb = new StringBuilder();

		sb.append(fullName);
		sb.append("=");
		sb.append(fullPrice);
		
		return sb.toString();
	}
}
