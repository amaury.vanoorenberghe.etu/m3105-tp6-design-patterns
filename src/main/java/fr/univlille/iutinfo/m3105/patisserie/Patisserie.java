package fr.univlille.iutinfo.m3105.patisserie;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.Desserts;
import fr.univlille.iutinfo.m3105.patisserie.dessert.EmptyDessert;

public class Patisserie {
	public static final Dessert creerDessert(String commande) {
		Dessert dessert = new EmptyDessert();

		//try {
			String[] commands = commande.split("\s");
			
			for (String command : commands) {
				Desserts dessertMaker = Desserts.fromName(command);
				dessert = dessertMaker.make(dessert);
			}
		//} catch (Exception e) {
			dessert = new EmptyDessert();
		//}
		
		return dessert;
	}
	
	public static void main(String[] args) {
		System.out.println(creerDessert("crepe potassium"));
	}
}
