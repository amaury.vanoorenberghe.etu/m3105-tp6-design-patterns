package fr.univlille.iutinfo.m3105.patisserie.dessert;

/**
 * Garniture
 */
public abstract class Topping extends Dessert {
	private final Dessert DESSERT;
	
	public Topping(Dessert base) {
		DESSERT = base;
	}
	
	public abstract double extraPrice();
	public abstract String nameFormat();
	
	@Override
	public double price() {
		return extraPrice() + DESSERT.price();
	}

	@Override
	public String name() {
		return String.format(nameFormat(), DESSERT.name());
	}
}
