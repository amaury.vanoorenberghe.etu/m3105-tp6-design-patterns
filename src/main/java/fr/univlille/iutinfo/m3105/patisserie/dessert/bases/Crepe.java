package fr.univlille.iutinfo.m3105.patisserie.dessert.bases;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;

/**
 * Crêpe
 */
public class Crepe extends Dessert {
	@Override
	public double price() {
		return 0.80;
	}

	@Override
	public String name() {
		return "Crêpe";
	}

	@Override
	public String[] identifier() {
		return new String[] {"crepe"};
	}
}