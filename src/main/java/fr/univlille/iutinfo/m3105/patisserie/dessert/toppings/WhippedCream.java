package fr.univlille.iutinfo.m3105.patisserie.dessert.toppings;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.Topping;

/**
 * Chantilly
 */
public class WhippedCream extends Topping {
	public WhippedCream(Dessert base) {
		super(base);
	}

	@Override
	public double extraPrice() {
		return 0.95;
	}

	@Override
	public String nameFormat() {
		return "%s avec de la chantilly";
	}

	@Override
	public String[] identifier() {
		return new String[] {"chantilly", "whipped-cream"};
	}
}
