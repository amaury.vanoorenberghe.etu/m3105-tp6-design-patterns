package fr.univlille.iutinfo.m3105.patisserie.printers;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;

public abstract class DessertPrinter {
	public abstract String print(Dessert dessert);
}
