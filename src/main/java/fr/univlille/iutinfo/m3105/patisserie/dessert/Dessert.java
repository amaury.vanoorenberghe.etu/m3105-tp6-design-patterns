package fr.univlille.iutinfo.m3105.patisserie.dessert;

import java.util.Locale;

/**
 * Dessert
 */
public abstract class Dessert {
	public abstract double price();
	public abstract String name();
	
	public abstract String[] identifier();
	
	@Override
	public String toString() {
		final String fullPrice = String.format(Locale.ROOT, "%.2f€", price());
		final String fullName = name();
		
		return fullName + " : " + fullPrice;
	}
}