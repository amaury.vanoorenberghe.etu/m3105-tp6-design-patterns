package fr.univlille.iutinfo.m3105.patisserie;

import java.util.*;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.EmptyDessert;

public class Assortment implements Iterable<Dessert> {
	private final List<Dessert> DESSERTS;
	
	public Assortment(Dessert... desserts) {
		DESSERTS = new ArrayList<Dessert>();
		DESSERTS.addAll(Arrays.asList(desserts));
	}
	
	public boolean add(Dessert dessert) {
		return DESSERTS.add(dessert);
	}
	
	public boolean add(String commande) {
		final Dessert dessert = Patisserie.creerDessert(commande);
		
		if (dessert == null || dessert.getClass() == EmptyDessert.class) {
			return false;
		}
		
		return add(dessert);
	}
	
	@Override
	public Iterator<Dessert> iterator() {
		return DESSERTS.iterator();
	}

}
