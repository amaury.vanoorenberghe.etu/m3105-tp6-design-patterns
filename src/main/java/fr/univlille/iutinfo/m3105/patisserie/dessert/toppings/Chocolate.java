package fr.univlille.iutinfo.m3105.patisserie.dessert.toppings;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.Topping;

/**
 * Chocolat
 */
public class Chocolate extends Topping {
	public Chocolate(Dessert base) {
		super(base);
	}

	@Override
	public double extraPrice() {
		return 1.50;
	}

	@Override
	public String nameFormat() {
		return "%s au chocolat";
	}

	@Override
	public String[] identifier() {
		return new String[] {"chocolat", "chocolate", "choco"};
	}
}