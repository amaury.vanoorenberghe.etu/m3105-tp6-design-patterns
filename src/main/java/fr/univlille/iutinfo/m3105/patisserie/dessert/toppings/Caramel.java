package fr.univlille.iutinfo.m3105.patisserie.dessert.toppings;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.Topping;

/**
 * Caramel
 */
public class Caramel extends Topping {
	public Caramel(Dessert base) {
		super(base);
	}

	@Override
	public double extraPrice() {
		return 0.6;
	}

	@Override
	public String nameFormat() {
		return "%s au caramel";
	}

	@Override
	public String[] identifier() {
		return new String[] {"caramel"};
	}
}
