package fr.univlille.iutinfo.m3105.patisserie.dessert.bases;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;

/**
 * Gauffre
 */
public class Waffle extends Dessert {
	@Override
	public double price() {
		return 0.80;
	}

	@Override
	public String name() {
		return "Gauffre";
	}

	@Override
	public String[] identifier() {
		return new String[] {"gauffre", "waffle"};
	}
}