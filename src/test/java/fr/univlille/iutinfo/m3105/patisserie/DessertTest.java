package fr.univlille.iutinfo.m3105.patisserie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.univlille.iutinfo.m3105.patisserie.dessert.Dessert;
import fr.univlille.iutinfo.m3105.patisserie.dessert.bases.Crepe;
import fr.univlille.iutinfo.m3105.patisserie.dessert.bases.Waffle;
import fr.univlille.iutinfo.m3105.patisserie.dessert.toppings.Chocolate;
import fr.univlille.iutinfo.m3105.patisserie.dessert.toppings.Strawberries;
import fr.univlille.iutinfo.m3105.patisserie.dessert.toppings.WhippedCream;

/**
 * Tuto cuisine
 */
public class DessertTest {
	private Dessert 
		crepe,
		waffle,
		creamCrepe,
		chocoWaffle,
		doubleChocoCrepe,
		strawberryChocoWaffle;
	
	@BeforeEach
	protected void setup() {
		crepe = new Crepe();
		waffle = new Waffle();
		chocoWaffle = new Chocolate(waffle);
		creamCrepe = new WhippedCream(crepe);
		doubleChocoCrepe = new Chocolate(new Chocolate(crepe));
		strawberryChocoWaffle = new Strawberries(chocoWaffle);
	}
	
	@Test
	protected void get_dessert_tostring() {
		assertEquals("Crêpe : 0.80€", crepe.toString());
		assertEquals("Gauffre : 0.80€", waffle.toString());
		
		assertEquals("Gauffre au chocolat : 2.30€", chocoWaffle.toString());
		assertEquals("Crêpe avec de la chantilly : 1.75€", creamCrepe.toString());
		assertEquals("Crêpe au chocolat au chocolat : 3.80€", doubleChocoCrepe.toString());		
		assertEquals("Gauffre au chocolat aux fraises : 3.55€", strawberryChocoWaffle.toString());
	}
}